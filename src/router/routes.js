import { Platform } from "quasar";

const children = [
  { path: "/home", redirect: "" },
  { path: "", redirect: "/read" },
  { path: "/create", component: () => import("pages/CreatePage.vue") },
  { path: "/read", component: () => import("pages/ReadPage.vue") },
  { path: "/library", component: () => import("pages/LibraryPage.vue") },
  { path: "/about", component: () => import("pages/AboutPage.vue") },
];

if (!Platform.is.electron) {
  children.push({
    path: "/download",
    component: () => import("pages/DownloadPage.vue"),
  });
}

const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: children,
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
