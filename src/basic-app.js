const { api, constants: c } = require("hyperpubee");

class App {
  constructor(hyperInterface, swarmInterface) {
    if (hyperInterface == null)
      throw new Error("An app must have a hyperInterface");

    this.hyperInterface = hyperInterface;
    this.swarmInterface = swarmInterface;
  }

  async createWorkFromText({ text, name, author }) {
    const pubee = await api.buildWorkFromText(
      text,
      name,
      this.hyperInterface,
      author
    );
    return pubee.key;
  }

  async serve(key) {
    const core = await this.hyperInterface.readHypercore(key);
    await this.swarmInterface.serveCore(core.discoveryKey);
  }

  async ensureCoreAvailable (key) { // This is a hack, no longer needed with redesign
    const core = await this.hyperInterface.readHypercore(key);
    if (core.length > 0) return
    await this.swarmInterface.requestCore(key)
    await core.get(0)
  }

  async get(key, location) {
    await this.ensureCoreAvailable(key)
    // TODO: consider whether to skip the swarm request when the
    // core is writable (thus local), as it is never necessary

    const res = await api.getEntry(key, location, this.hyperInterface);
    return res;
  }

  async getRoot(key) {
    await this.ensureCoreAvailable(key)

    const res = await api.getRoot(key, this.hyperInterface);
    return res;
  }

  async getAuthor(key) {
    await this.ensureCoreAvailable(key)

    const metadata = await this.get(key, c.METADATA);
    return metadata.author;
  }

  async ready() {
    await this.hyperInterface.ready();
  }

  async getAllEmbeddings(key) {
    await this.ensureCoreAvailable(key)
    return await api.getAllEmbeddings(key, this.hyperInterface);
  }

  async close() {
    await Promise.all([
      this.hyperInterface.close(),
      this.swarmInterface.close(),
    ]);
  }
}

module.exports = App;
