function getQuotedRangesOfEmbedding(embedding) {
  if (embedding.patch === undefined) {
    // No embedding
    return [];
  }

  if (embedding.patch.length === 0) {
    // All the text is embedded
    return [[0, undefined]];
  }
  // Else: there are patches

  const quotedRanges = [];
  let startIndex = 0;

  for (const patch of embedding.patch) {
    const operation = patch[0];
    const text = patch[1];

    if (operation === -1) {
      // Skipping text in the referenced work
      startIndex += text.length;
    } else if (operation === 0) {
      // Quoted text
      const endIndex = startIndex + text.length;
      quotedRanges.push([startIndex, endIndex]);
      startIndex += text.length;
    } else {
      // Text not present in the referenced work (or invalid operation)
    }
  }

  return quotedRanges;
}

function getTextSpansFromQuotedRanges({
  text,
  quotedTextRanges,
  includeValue,
  excludeValue = "",
}) {
  if (quotedTextRanges.length === 0) {
    return [[text, excludeValue]];
  }

  const res = [];
  let contentNextI = 0;

  for (const range of quotedTextRanges) {
    const preSlice = text.slice(contentNextI, range[0]);
    const quotedSlice = text.slice(...range);

    res.push([preSlice, excludeValue]);
    res.push([quotedSlice, includeValue]);

    contentNextI = range[1];
  }

  if (contentNextI !== undefined) {
    // Undefined means the last range ended at end of string
    const remainingText = text.slice(contentNextI);
    res.push([remainingText, excludeValue]);
  }

  return res;
}

module.exports = {
  getQuotedRangesOfEmbedding,
  getTextSpansFromQuotedRanges,
};
