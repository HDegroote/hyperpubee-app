const utils = require("./utils");

class QuoteBackReference {
  constructor({ location, quotedIndices }) {
    this.location = location;
    this.quotedIndices = quotedIndices;
  }

  static fromEmbedding(embedding) {
    const quotedIndices = utils.getQuotedRangesOfEmbedding(embedding);

    return new QuoteBackReference({
      location: embedding.referencedLocation,
      quotedIndices,
    });
  }
}

module.exports = {
  QuoteBackReference,
};
