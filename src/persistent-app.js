const App = require("./basic-app");
const { utils, helpers, api } = require("hyperpubee");

class PersistentApp extends App {
  constructor({ dbInterface, corestorePath, hyperInterface, swarmInterface }) {
    super(hyperInterface, swarmInterface);

    this.dbInterface = dbInterface;
    this.corestorePath = corestorePath;
  }

  async createWorkFromText({ name, ...params }) {
    const workKey = await super.createWorkFromText({ name, ...params });

    this.dbInterface.addCore({
      name,
      key: workKey,
      corestorePath: this.corestorePath,
    });

    return workKey;
  }

  async getAllDiscoveryKeys() {
    const keys = this.getStoredWorks().map((dbCore) => dbCore.key);

    const promises = keys.map(
      async (key) => (await this.hyperInterface.readHypercore(key)).discoveryKey
    );
    const discoveryKeys = await Promise.all(promises);

    return discoveryKeys;
  }

  _getDbCore(key) {
    const dbCore = this.dbInterface.getCoreByKey(key);

    if (dbCore === undefined) {
      throw new Error(
        [
          `Cannot manage a work which does not belong to the library`,
          `(${key}--corestore '${this.corestorePath}')`,
        ].join(" ")
      );
    }

    return dbCore;
  }

  _ensureKeyIsInLibrary(key) {
    this._getDbCore(key); // Throws if not in lib
  }

  async serveWork(key) {
    this._ensureKeyIsInLibrary(key);

    const core = await this.hyperInterface.readHypercore(key);
    await this.swarmInterface.serveCore(core.discoveryKey);
  }

  async serveAllWorks() {
    const discoveryKeys = await this.getAllDiscoveryKeys();
    await this.swarmInterface.serveCores(discoveryKeys);
  }

  async unserveWork(key) {
    this._ensureKeyIsInLibrary(key);

    const core = await this.hyperInterface.readHypercore(key);
    await this.swarmInterface.unserveCore(core.discoveryKey);
  }

  async unserveAllWorks() {
    const discoveryKeys = await this.getAllDiscoveryKeys();
    await this.swarmInterface.unserveCores(discoveryKeys);
  }

  getStoredWorks() {
    return this.dbInterface.getCoresOfCorestore(this.corestorePath);
  }

  async getLibrary() {
    const dbWorks = this.getStoredWorks();
    const hostedWorks = this.swarmInterface.servedDiscoveryKeys;

    const works = [];
    for (const dbWork of dbWorks) {
      const pubee = await api.readWork(dbWork.key, this.hyperInterface);
      const core = pubee.bee.feed;

      const hosted = hostedWorks.includes(utils.getKeyAsStr(core.discoveryKey));

      const work = new LibraryWork({
        name: dbWork.name,
        title: await helpers.getTitleAsText(pubee),
        hash: dbWork.key,
        writable: core.writable,
        hosted,
      });

      works.push(work);
    }

    return new Library(works);
  }
}

class LibraryWork {
  constructor({ name, title, hash, writable, hosted }) {
    this.name = name;
    this.title = title;
    this.hash = hash;
    this.writable = writable;
    this.hosted = hosted;
  }
}
class Library {
  constructor(works) {
    this.works = works;
  }
}

module.exports = {
  PersistentApp,
  Library,
  LibraryWork,
};
