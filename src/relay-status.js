const StatusEnum = Object.freeze({
  NOT_CONNECTED: Symbol("Not connected"),
  CONNECTED: Symbol("Connected"),
  CONNECTING: Symbol("Connecting"),
});

class RelayStatus {
  constructor(initStatus) {
    this.setStatus(initStatus);
  }

  setStatus(status) {
    if (!Object.values(StatusEnum).includes(status)) {
      throw new Error("status should be a StatusEnum");
    }
    this.status = status;
  }

  isConnected() {
    return this.status === StatusEnum.CONNECTED;
  }

  isConnecting() {
    return this.status === StatusEnum.CONNECTING;
  }

  isNotConnected() {
    return this.status === StatusEnum.NOT_CONNECTED;
  }

  get statusStr() {
    return this.status.description;
  }
}

module.exports = { RelayStatus, StatusEnum };
