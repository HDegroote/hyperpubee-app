const { strict: nodeAssert } = require("assert");
const { expect } = require("chai");

const { persistentAppFactory } = require("./fixtures");
const { utils, constants: c } = require("hyperpubee");
const { Library, LibraryWork } = require("../src/persistent-app");

describe("persistent-app tests", function () {
  let app1, destroy;

  this.beforeEach(async function () {
    ({ app1, destroy } = await persistentAppFactory());
  });

  this.afterEach(async function () {
    await destroy();
  });

  it("Can create a work with an author", async function () {
    const text = "[prose]Some piece of prose";
    const key = await app1.createWorkFromText({
      text,
      name: "test prose",
      author: "Fitz G.",
    });

    const { author } = await app1.get(key, c.METADATA);
    expect(author).to.equal("Fitz G.");
  });

  it("stores created poems in the db", async function () {
    const initCores = await app1.getStoredWorks();
    expect(initCores).to.deep.equal([]);

    await app1.createWorkFromText({
      text: "[poetry]A poem\nIs here",
      name: "Test poem",
    });

    const cores = await app1.getStoredWorks();
    expect(cores.length).to.equal(1);
  });

  it("can serve all available works", async function () {
    // A bit hacky, but this simulates the app containing 2 cores,
    // even though they are empty hypercores rather than actual works
    const core1 = await app1.hyperInterface.createHypercore("core1");
    const core2 = await app1.hyperInterface.createHypercore("core2");

    app1.dbInterface.addCore({
      name: "core1",
      key: core1.key,
      corestorePath: app1.corestorePath,
    });
    app1.dbInterface.addCore({
      name: "core2",
      key: core2.key,
      corestorePath: app1.corestorePath,
    });

    await app1.serveAllWorks();

    const replicatedKeys = Array.from(
      app1.swarmInterface.replicatedDiscoveryKeys
    );
    const expectedKeys = [
      utils.getKeyAsStr(core1.discoveryKey),
      utils.getKeyAsStr(core2.discoveryKey),
    ];

    expect(replicatedKeys).to.have.members(expectedKeys);
  });

  it("Throws when attempting to serve a core not present in db", async function () {
    await nodeAssert.rejects(app1.serveWork("a".repeat(64)), {
      message: [
        "Cannot manage a work which does not belong to the library ",
        "(aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "--corestore 'ram1')",
      ].join(""),
    });
  });

  it("Can serve a work", async function () {
    expect(app1.swarmInterface.replicatedDiscoveryKeys.length).to.equal(0);

    const key = await app1.createWorkFromText({
      text: "[poetry]A poem\nIs here",
      name: "Test poem",
    });
    await app1.serveWork(key);

    expect(app1.swarmInterface.replicatedDiscoveryKeys.length).to.equal(1);
  });

  it("Can return the correct library", async function () {
    const key1 = await app1.createWorkFromText({
      text: "[poetry]A poem\nIs here",
      name: "poem 1",
    });

    const key2 = await app1.createWorkFromText({
      text: "[poetry]#with a title\n\nAnother one\nIs here",
      name: "poem 2",
    });

    await app1.serveWork(key1);

    const actualLibrary = await app1.getLibrary();

    const expectedWorks = [
      new LibraryWork({
        name: "poem 1",
        title: "",
        hash: key1,
        writable: true,
        hosted: true,
      }),
      new LibraryWork({
        name: "poem 2",
        title: "with a title",
        hash: key2,
        writable: true,
        hosted: false,
      }),
    ];
    const expectedLibrary = new Library(expectedWorks);
    expect(actualLibrary).to.deep.equal(expectedLibrary);
  });

  describe("Test unserving logic", function () {
    let key;

    this.beforeEach(async function () {
      key = await app1.createWorkFromText({
        text: "[poetry]A poem\nIs here",
        name: "Test poem",
      });

      // We don't need the second key for these tests
      await app1.createWorkFromText({
        text: "[poetry]Another poem\nIs here",
        name: "Test poem 2",
      });

      // Serve 2 works
      await app1.serveAllWorks();
    });

    it("Can unserve a work", async function () {
      expect(app1.swarmInterface.replicatedDiscoveryKeys.length).to.equal(2);

      await app1.unserveWork(key);
      expect(app1.swarmInterface.replicatedDiscoveryKeys.length).to.equal(1);
    });

    it("Can unserve all works", async function () {
      expect(app1.swarmInterface.replicatedDiscoveryKeys.length).to.equal(2);

      await app1.unserveAllWorks();
      expect(app1.swarmInterface.replicatedDiscoveryKeys.length).to.equal(0);
    });
  });
});
