const { expect } = require("chai");

const { contentAtIKey } = require("./helpers.js");
const { basicAppFactory } = require("./fixtures");

describe("App tests", function () {
  let app1, app2, destroy;
  let poemKey;
  const expectedLine = "A poem";

  this.beforeEach(async function () {
    ({ app1, app2, destroy } = await basicAppFactory());

    const text = "[poetry]A poem\nOf 2 lines";
    poemKey = await app1.createWorkFromText({
      text,
      name: "test poem",
      author: "FitzG",
    });
  });

  this.afterEach(async function () {
    await destroy();
  });

  it("Can get the root of a poem", async function () {
    const root = await app1.getRoot(poemKey);
    expect(root.length).to.equal(1); // The poem struct
  });

  it("Can get a value of a part of a poem", async function () {
    location = contentAtIKey(0);
    const value = await app1.get(poemKey, location);

    expect(value).to.equal(expectedLine); // The poem
  });

  it("Can get the author of a poem", async function () {
    const author = await app1.getAuthor(poemKey);
    expect(author).to.equal("FitzG");
  });

  it("Can get the root of a peer-hosted poem", async function () {
    await app1.serve(poemKey);

    const root = await app2.getRoot(poemKey);
    expect(root.length).to.equal(1);
  });

  it("Can get a value of a part of a peer-hosted poem", async function () {
    await app1.serve(poemKey);

    location = contentAtIKey(0);
    const value = await app2.get(poemKey, location);

    expect(value).to.equal(expectedLine);
  });

  it("Can create prose", async function () {
    const text = "[prose]Some piece of prose";
    const key = await app1.createWorkFromText({
      text,
      name: "test prose",
    });

    const root = await app1.getRoot(key);
    expect(root.length).to.equal(1); // Sanity check
    expect(root[0].includes("prose")).to.equal(true); // Format: structure.prose.0
  });

  it("Can get all embeddings", async function () {
    const text = `[poetry]
    Embed <${"a".repeat(64)}/content/0>
    More <${"b".repeat(64)}/content/4>`;
    const key = await app1.createWorkFromText({
      text,
      name: "test work",
    });

    const embeddings = await app1.getAllEmbeddings(key);
    expect(embeddings.length).to.deep.equal(2);
  });
});
