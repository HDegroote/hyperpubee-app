const { constants: c, utils } = require("hyperpubee");

function contentAtIKey(index) {
  location = utils.createHexLexedReference(c.CONTENT, index);
  return location;
}

module.exports = {
  contentAtIKey,
};
