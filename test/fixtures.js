const createTestnet = require("@hyperswarm/testnet");
const SwarmInterface = require("hyperpubee-swarm-interface");
const Corestore = require("corestore");
const ram = require("random-access-memory");
const HyperInterface = require("hyperpubee-hyper-interface");
const Hyperswarm = require("hyperswarm");
const DbInterface = require("corestore-metadata-db");
const sqlite3 = require("better-sqlite3");

const BasicApp = require("../src/basic-app");
const { PersistentApp } = require("../src/persistent-app");

async function testnetFactory(corestore1, corestore2) {
  const testnet = await createTestnet(3);
  const bootstrap = testnet.bootstrap;

  const swarmInterface1 = new SwarmInterface(
    new Hyperswarm(bootstrap),
    corestore1
  );
  const swarmInterface2 = new SwarmInterface(
    new Hyperswarm(bootstrap),
    corestore2
  );

  async function destroyTestnetFactory() {
    await Promise.all([
      swarmInterface2.close(),
      swarmInterface1.close(),
      testnet.destroy(),
    ]);
  }

  return {
    testnet: testnet,
    bootstrap: bootstrap,
    swarmInterface1: swarmInterface1,
    swarmInterface2: swarmInterface2,
    destroy: destroyTestnetFactory,
  };
}

async function hyperInterfaceFactory() {
  const corestore = new Corestore(ram);
  await corestore.ready();

  const hyperInterface = new HyperInterface(corestore);
  return hyperInterface;
}

function dbInterfaceFactory() {
  const db = sqlite3(":memory:");
  const dbInterface = new DbInterface(db);

  return dbInterface;
}

async function basicAppFactory() {
  const promises = [hyperInterfaceFactory(), hyperInterfaceFactory()];
  const [hyperInterface1, hyperInterface2] = await Promise.all(promises);

  const testnet = await testnetFactory(
    hyperInterface1.corestore,
    hyperInterface2.corestore
  );

  const app1 = new BasicApp(hyperInterface1, testnet.swarmInterface1);
  const app2 = new BasicApp(hyperInterface2, testnet.swarmInterface2);

  await app1.ready();
  await app2.ready();

  async function destroy() {
    await Promise.all([app1.close(), app2.close(), testnet.destroy()]);
  }

  return {
    app1,
    app2,
    destroy,
  };
}

async function persistentAppFactory() {
  const [hyperInterface1, hyperInterface2] = await Promise.all([
    hyperInterfaceFactory(),
    hyperInterfaceFactory(),
  ]);

  const testnet = await testnetFactory(
    hyperInterface1.corestore,
    hyperInterface2.corestore
  );

  const db1 = dbInterfaceFactory();
  const db2 = dbInterfaceFactory();

  const app1 = new PersistentApp({
    dbInterface: db1,
    corestorePath: "ram1",
    hyperInterface: hyperInterface1,
    swarmInterface: testnet.swarmInterface1,
  });
  const app2 = new PersistentApp({
    dbInterface: db2,
    corestorePath: "ram2",
    hyperInterface: hyperInterface2,
    swarmInterface: testnet.swarmInterface2,
  });

  async function destroy() {
    await Promise.all([
      testnet.destroy(),
      hyperInterface1.close(),
      hyperInterface2.close(),
    ]);
  }

  return {
    app1,
    app2,
    destroy,
  };
}

module.exports = {
  testnetFactory,
  hyperInterfaceFactory,
  dbInterfaceFactory,
  basicAppFactory,
  persistentAppFactory,
};
