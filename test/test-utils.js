const { expect } = require("chai");
const { utils: pubeeUtils, constants: c } = require("hyperpubee");
const { Embedding } = require("hyperpubee");

const {
  getQuotedRangesOfEmbedding,
  getTextSpansFromQuotedRanges,
} = require("../src/utils");

describe("Test getQuotedRangesOfEmbedding", function () {
  let embedding;
  const text = "I am a text here";

  this.beforeEach(async function () {
    const referencedLocation = pubeeUtils.getIndexedKeyFromComponents([
      c.CONTENT,
      1,
    ]);

    embedding = new Embedding({
      referencedHash: "a".repeat(64),
      referencedLocation,
      patch: [],
    });
  });

  it("Returns one range which contains all text when no patches are specified", async function () {
    const ranges = getQuotedRangesOfEmbedding(embedding);

    expect(ranges.length).to.equal(1);
    expect(text.slice(...ranges[0])).to.equal("I am a text here");
  });

  it("Returns an empty array when the passed object has no patch", async function () {
    delete embedding.patch;
    const ranges = getQuotedRangesOfEmbedding(embedding);

    expect(ranges.length).to.equal(0);
  });

  it("Returns the verbatim quoted parts of the text when there is a patch", async function () {
    embedding.patch = [
      [0, "I "],
      [1, "Something else"],
      [-1, "am"],
      [0, " a text "],
      [-1, "here"],
    ];
    const ranges = getQuotedRangesOfEmbedding(embedding);

    expect(ranges.length).to.equal(2);
    expect(text.slice(...ranges[0])).to.equal("I ");
    expect(text.slice(...ranges[1])).to.equal(" a text ");
  });
});

describe("Test getTextSpansFromQuotedRanges", function () {
  const text = "I am a text here";
  const includeValue = true;
  const excludeValue = false;

  it("Returns the correct spans if quotes are present", function () {
    const quotedTextRanges = [
      [0, 2],
      [4, 12],
    ];

    const actual = getTextSpansFromQuotedRanges({
      text,
      quotedTextRanges,
      includeValue,
      excludeValue,
    });

    const expected = [
      ["", false], // Current implementation always starts with excluded Value
      ["I ", true],
      ["am", false],
      [" a text ", true],
      ["here", false],
    ];

    expect(actual).to.deep.equal(expected);
  });

  it("Returns all text as excluded if no quotes are present", function () {
    const quotedTextRanges = [];

    const actual = getTextSpansFromQuotedRanges({
      text,
      quotedTextRanges,
      includeValue,
      excludeValue,
    });

    const expected = [["I am a text here", false]];
    expect(actual).to.deep.equal(expected);
  });
});
