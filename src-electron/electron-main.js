import path from "path";
import os from "os";
const fs = require("fs");

import { app, BrowserWindow, nativeTheme, ipcMain } from "electron";
const HyperInterface = require("hyperpubee-hyper-interface");
const sqlite3 = require("better-sqlite3");
const Corestore = require("corestore");
const ram = require("random-access-memory");
const Hyperswarm = require("hyperswarm");
const SwarmInterface = require("hyperpubee-swarm-interface");
const { loadConfig, APPLICATION_NAME } = require("./config");
const DbInterface = require("corestore-metadata-db");

const { PersistentApp } = require("../src/persistent-app");
const FrontendApp = require("../src/basic-app");

// needed in case process is undefined under Linux
const platform = process.platform || os.platform();

try {
  if (platform === "win32" && nativeTheme.shouldUseDarkColors === true) {
    require("fs").unlinkSync(
      path.join(app.getPath("userData"), "DevTools Extensions")
    );
  }
} catch (_) {
  console.log("Caught error when checking platform === win32");
}

let mainWindow;
let persistentApp, nonPersistentApp;

app.whenReady().then(() => {
  const config = getConfig();
  console.log("Using config: ", config);

  nonPersistentApp = setupNonPersistentApp();
  setupNonPersistentAppIPCListeners();

  persistentApp = setupPersistentApp(config);
  setupPersistentAppIPCListeners();

  createWindow();
});

function getConfig() {
  const defaultDataDir = path.join(app.getPath("appData"), APPLICATION_NAME);
  if (!fs.existsSync(defaultDataDir)) {
    fs.mkdirSync(defaultDataDir);
  }

  return loadConfig({ defaultDataDir });
}

app.on("window-all-closed", async () => {
  console.log("Shutting down non-persistent app");
  await nonPersistentApp.close();

  console.log("Shutting down persistent app");
  await persistentApp.close();

  if (platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    icon: path.resolve(__dirname, "icons/icon.png"), // tray icon
    width: 1000,
    height: 600,
    useContentSize: true,
    webPreferences: {
      contextIsolation: true,
      // More info: /quasar-cli/developing-electron-apps/electron-preload-script
      preload: path.resolve(__dirname, process.env.QUASAR_ELECTRON_PRELOAD),
    },
  });

  mainWindow.loadURL(process.env.APP_URL);

  if (process.env.DEBUGGING) {
    // if on DEV or Production with debug enabled
    mainWindow.webContents.openDevTools();
  } else {
    // we're on production; no access to devtools pls
    mainWindow.webContents.on("devtools-opened", () => {
      mainWindow.webContents.closeDevTools();
    });
  }

  mainWindow.on("closed", () => {
    mainWindow = null;
  });
}

function setupPersistentApp(config) {
  const db = sqlite3(config.DB_CONNECTION_STR);

  const corestorePath = config.CORESTORE_LOC;
  const corestore = new Corestore(corestorePath);

  const hyperInterface = new HyperInterface(corestore);
  const dbInterface = new DbInterface(db);

  const swarm = new Hyperswarm();
  const swarmInterface = new SwarmInterface(swarm, corestore);

  const frontendApp = new PersistentApp({
    dbInterface,
    corestorePath,
    hyperInterface,
    swarmInterface,
  });

  return frontendApp;
}

function setupNonPersistentApp() {
  const nonPersistentCorestore = new Corestore(ram);
  const nonPersistentHyperInterface = new HyperInterface(
    nonPersistentCorestore
  );

  const nonPersistentSwarm = new Hyperswarm();
  const nonPersistentSwarmInterface = new SwarmInterface(
    nonPersistentSwarm,
    nonPersistentCorestore
  );

  const nonPersistentApp = new FrontendApp(
    nonPersistentHyperInterface,
    nonPersistentSwarmInterface
  );

  return nonPersistentApp;
}

function setupPersistentAppIPCListeners() {
  ipcMain.handle("persistent:ready", async function (event, ...args) {
    return await persistentApp.ready(...args);
  });

  ipcMain.handle("persistent:get", async function (event, ...args) {
    return await persistentApp.get(...args);
  });

  ipcMain.handle("persistent:getRoot", async function (event, ...args) {
    return await persistentApp.getRoot(...args);
  });

  ipcMain.handle("persistent:getAuthor", async function (event, ...args) {
    return await persistentApp.getAuthor(...args);
  });

  ipcMain.handle(
    "persistent:createWorkFromText",
    async function (event, ...args) {
      return await persistentApp.createWorkFromText(...args);
    }
  );

  ipcMain.handle(
    "persistent:getAllEmbeddings",
    async function (event, ...args) {
      return await persistentApp.getAllEmbeddings(...args);
    }
  );

  ipcMain.handle("persistent:getLibrary", async function (event, ...args) {
    return await persistentApp.getLibrary(...args);
  });

  ipcMain.handle("persistent:serveWork", async function (event, ...args) {
    return await persistentApp.serveWork(...args);
  });

  ipcMain.handle("persistent:serveAllWorks", async function (event, ...args) {
    return await persistentApp.serveAllWorks(...args);
  });

  ipcMain.handle("persistent:unserveWork", async function (event, ...args) {
    return await persistentApp.unserveWork(...args);
  });

  ipcMain.handle("persistent:unserveAllWorks", async function (event, ...args) {
    return await persistentApp.unserveAllWorks(...args);
  });
}

function setupNonPersistentAppIPCListeners() {
  ipcMain.handle("nonPersistent:ready", async function (event, ...args) {
    return await nonPersistentApp.ready(...args);
  });

  ipcMain.handle("nonPersistent:get", async function (event, ...args) {
    return await nonPersistentApp.get(...args);
  });

  ipcMain.handle("nonPersistent:getRoot", async function (event, ...args) {
    return await nonPersistentApp.getRoot(...args);
  });

  ipcMain.handle("nonPersistent:getAuthor", async function (event, ...args) {
    return await nonPersistentApp.getAuthor(...args);
  });

  ipcMain.handle(
    "nonPersistent:createWorkFromText",
    async function (event, ...args) {
      return await nonPersistentApp.createWorkFromText(...args);
    }
  );

  ipcMain.handle(
    "nonPersistent:getAllEmbeddings",
    async function (event, ...args) {
      return await nonPersistentApp.getAllEmbeddings(...args);
    }
  );
}
