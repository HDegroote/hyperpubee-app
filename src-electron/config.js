const rc = require("rc");
const path = require("node:path");

const APPLICATION_NAME = "hyperpubee";
const DEFAULT_DB_FILENAME = "hyperpubee.db";
const DEFAULT_CORESTORE_DIRNAME = "my-hyperpubees";

function loadConfig({ defaultDataDir }) {
  // Note: the defaults are only used when the user did not specify
  // anything else
  const defaultDbConnectionStr = path.join(defaultDataDir, DEFAULT_DB_FILENAME);
  const defaultCorestoreLoc = path.join(
    defaultDataDir,
    DEFAULT_CORESTORE_DIRNAME
  );

  const config = rc(APPLICATION_NAME, {
    DB_CONNECTION_STR: defaultDbConnectionStr,
    CORESTORE_LOC: defaultCorestoreLoc,
  });

  return config;
}

module.exports = { loadConfig, APPLICATION_NAME };
