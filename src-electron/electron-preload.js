const { contextBridge, ipcRenderer } = require("electron");

contextBridge.exposeInMainWorld("electron", {
  persistent: {
    ready: (...args) => ipcRenderer.invoke("persistent:ready", ...args),

    get: (...args) => ipcRenderer.invoke("persistent:get", ...args),

    getRoot: (...args) => ipcRenderer.invoke("persistent:getRoot", ...args),

    getAuthor: (...args) => ipcRenderer.invoke("persistent:getAuthor", ...args),

    createWorkFromText: (...args) =>
      ipcRenderer.invoke("persistent:createWorkFromText", ...args),

    close: (...args) => ipcRenderer.invoke("persistent:close", ...args),

    getAllEmbeddings: (...args) =>
      ipcRenderer.invoke("persistent:getAllEmbeddings", ...args),

    getLibrary: (...args) =>
      ipcRenderer.invoke("persistent:getLibrary", ...args),

    serveWork: (...args) => ipcRenderer.invoke("persistent:serveWork", ...args),

    serveAllWorks: (...args) =>
      ipcRenderer.invoke("persistent:serveAllWorks", ...args),

    unserveWork: (...args) =>
      ipcRenderer.invoke("persistent:unserveWork", ...args),

    unserveAllWorks: (...args) =>
      ipcRenderer.invoke("persistent:unserveAllWorks", ...args),
  },
  nonPersistent: {
    ready: (...args) => ipcRenderer.invoke("nonPersistent:ready", ...args),

    get: (...args) => ipcRenderer.invoke("nonPersistent:get", ...args),

    getRoot: (...args) => ipcRenderer.invoke("nonPersistent:getRoot", ...args),

    getAuthor: (...args) =>
      ipcRenderer.invoke("nonPersistent:getAuthor", ...args),

    createWorkFromText: (...args) =>
      ipcRenderer.invoke("nonPersistent:createWorkFromText", ...args),

    getAllEmbeddings: (...args) =>
      ipcRenderer.invoke("nonPersistent:getAllEmbeddings", ...args),
  },
});
