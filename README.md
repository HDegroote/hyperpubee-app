# HyperPubee App

A single-page application and Electron app to read and manage [HyperPubees](https://gitlab.com/HDegroote/hyperpubee).

The single-page application is hosted at https://www.hyperpubee.org, where you can also download the Electron app.

## Install

Clone the repository, then

```
yarn install
```

Note: yarn and quasar will need to be installed:

```
npm install --global yarn
```

```
yarn global add @quasar/cli
```

You might need to add the yarn dir with globally available packages to your path
(https://classic.yarnpkg.com/lang/en/docs/cli/global/)

## Run/Build Single-Page Application

A .env file is read from the app root directory when the app is built or runs in dev mode.
It requires a value for `HYPERPUBEE_RELAY_SERVER_URL` to be able to connect with a relay server. For example: `HYPERPUBEE_RELAY_SERVER_URL="wss://myserver.org:8443"`

To run locally:

```
quasar dev
```

To build:

```
quasar build
```

If there are better-sqlite3 compatibility issues, run

```
yarn upgrade better-sqlite3
```

Then try again

## Run/Build Electron

To run locally on Linux systems:

```
yarn upgrade better-sqlite3
yarn run electron-rebuild
quasar dev --mode electron
```

To build:

```
yarn upgrade better-sqlite3
yarn run electron-rebuild
quasar build --mode electron
```

The same commands apply to windows, except for the electron-rebuild step. Instead, use

yarn run electron-rebuild-win
